<!--
SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
SPDX-License-Identifier: CC-BY-4.0
-->

# General Information

Please note that this **is not** a usual Git repository.
Its main purpose is to demonstrate all relevant development steps of the accompanying workshop.
Consequently, branches are treated differently:
- Every branch except the default branch `main` shows the final results of a specific step.
- The default branch adds further documentation and automates checking some details.

## External Contributions 

1. **Create a fork** of  this GitLab project.
    - Integrate your contributions to the **earliest** appropriate branch.
1. **Create Merge Request**.
    - Request to merge into the branch you modified.
    - Provide information about your change.

After review, we will decide whether to integrate your changes.
This will be done in a manual process documented below.
Your merge request will be closed when the changes are integrated.
In addition, we will add your name to the [list of contributors](README.md) if you agree.
*Please be aware that this is our only option to track contributions. A later addition might not be possible.*

## Contribution Integration

> This section explains how to integrate contributions while keeping the commit and branch structure of the repository.
> If you are new to this process: It is highly recommended to practice the process by using a fork of this GitLab project!

### Preparation
- Clone the repository.
- Identify the branch and the relevant commits that should be changed.

### Apply the Changes
- Switch to the relevant branch.
- Integrate the changes (i.e., from the external merge request) into the relevant branch.
  For example, this can be done using an interactive rebase, amending to the last commit of the branch,
  or downloading and applying a patch from the merge request.
- Double check that the previous commit number and order is preserved.

### Rearrange the Reference Branches
- Rebase the `main` branch onto the modified branch.
- Run the script [reset_branches.sh](reset_branches.sh) to:
  - Point all workshop related branches onto the new linear history of `main`.
  - (Force) push all workshop related branches to GitLab.
  - Recreate the tag for the release of the example.
- Double check the current result and finalize the changes by (force) pushing the `main` branch.

> :bangbang: You might most probably want to drop the *old* unamended commit that you initially replaced to avoid conflicts.

> If relevant, please add new contributors to the [README.md](README.md).

### Example

In the following example, we update the reference date for the data retrieval.

#### Preparation
- The date is documented in the `README` file when it is initially introduced at the end of the third step.
- We therefore have to change the content of the last commit of the branch `3-add-documentation`.

#### Apply the Changes
- Perform the change:
  ```
  git switch 3-add-documentation
  # Adapt the README.md accordingly
  git add README.md
  git commit --amend
  ```
- Run `git log --graph --all --oneline --decorate` which should show something like:
```
* a345e7d (HEAD -> 3-add-documentation) Add basic description and usage information # new commit
| * f25a0f2 (origin/main, main) Update contribution process and add helper script from Johann
| * 7875430 Add contribution information
| * f98c126 (tag: 2024-03-20, origin/6-release-code, 6-release-code) Set DOI and release date
| * c4e8390 (origin/5-prepare-citation, 5-prepare-citation) Add citation hint and main contributors
| * 5c88e04 (origin/4-add-license-information, 4-add-license-information) Add license and copyright information
| * 53c87c6 Record license information of dependencies
| * fe37c56 (origin/3-add-documentation) Add basic description and usage information # old commit
|/
* e73c60d (origin/2-clean-up-code, 2-clean-up-code) Add a simple pipeline to check the code on every commit
* 33235b0 Clean up code in various ways
* 3715c50 Check code via linter, fix errors and lock dependencies
* fdfa30f Organize directory structure in accordance to a data analysis layout
* 6e47c5b Add a simple test script
* 589619f (origin/1-put-into-git, 1-put-into-git) Ignore temporary Python files and only allow plot files in the results folder
* 32b1364 Move plot files into a separate folder
* 3b8ad09 (origin/0-original-source, 0-original-source) Add initial version
```

### Rearrange the Reference Branches
- Rebase the `main` branch to update the logical following commits:
  ```
  git switch main
  git rebase -i 3-add-documentation
  # Drop the (old) `Add basic description and usage information` commit to avoid conflicts
  # Resolve possible conflicts (there should be no conflicts in this example)  
  ```
- Run `git log --graph --all --oneline --decorate` which should show something like:
```
* d986098 (HEAD -> main) Update contribution process and add helper script from Johann # latest rebased main commit
* 8793a94 Add contribution information
* 4bc6f88 Set DOI and release date
* 9f8668a Add citation hint and main contributors
* c9ea3e3 Add license and copyright information
* 5971764 Record license information of dependencies
* a345e7d (3-add-documentation) Add basic description and usage information # commit we changed initially
| * f25a0f2 (origin/main) Update contribution process and add helper script from Johann # old latest main commit on the remote branch
| * 7875430 Add contribution information
| * f98c126 (tag: 2024-03-20, origin/6-release-code, 6-release-code) Set DOI and release date
| * c4e8390 (origin/5-prepare-citation, 5-prepare-citation) Add citation hint and main contributors
| * 5c88e04 (origin/4-add-license-information, 4-add-license-information) Add license and copyright information
| * 53c87c6 Record license information of dependencies
| * fe37c56 (origin/3-add-documentation) Add basic description and usage information
|/
* e73c60d (origin/2-clean-up-code, 2-clean-up-code) Add a simple pipeline to check the code on every commit
* 33235b0 Clean up code in various ways
* 3715c50 Check code via linter, fix errors and lock dependencies
* fdfa30f Organize directory structure in accordance to a data analysis layout
* 6e47c5b Add a simple test script
* 589619f (origin/1-put-into-git, 1-put-into-git) Ignore temporary Python files and only allow plot files in the results folder
* 32b1364 Move plot files into a separate folder
* 3b8ad09 (origin/0-original-source, 0-original-source) Add initial version
```
- Now, all the following commits that we updated exist in two versions as the old versions are still present in the remote branches.
- Run `reset_branches.sh` to re-create the former branch structure and release tag:
```
Reset 7 branches (with --force)? (y/N):y
Pointing branch '6-release-code' to commit 'main~1'.
branch '6-release-code' set up to track 'origin/6-release-code'.
...
branch '0-original-source' set up to track 'origin/0-original-source'.
Push 7 branches with --force? (y/N):y
Enumerating objects: 51, done.
Counting objects: 100% (51/51), done.
Delta compression using up to 8 threads
Compressing objects: 100% (36/36), done.
Writing objects: 100% (37/37), 17.90 KiB | 5.97 MiB/s, done.
Total 37 (delta 17), reused 5 (delta 1), pack-reused 0
...
 + f98c126...4bc6f88 6-release-code -> 6-release-code (forced update)
...
 + c4e8390...9f8668a 5-prepare-citation -> 5-prepare-citation (forced update)
...
 + 5c88e04...c9ea3e3 4-add-license-information -> 4-add-license-information (forced update)
...
 + fe37c56...a345e7d 3-add-documentation -> 3-add-documentation (forced update)
Everything up-to-date # 2-... is up-to-date as nothing changed
Everything up-to-date # 1-... is up-to-date as nothing changed
Everything up-to-date # 0-... is up-to-date as nothing changed
Apply and push tag '2024-03-20' to stage '6-release-code' (with --force)? (y/N):y
Updated tag '2024-03-20' (was 4f88202)
...
 + 4f88202...617deba 2024-03-20 -> 2024-03-20 (forced update)
```
- Run `git log --graph --all --oneline --decorate` which should show something like:
```
* d986098 (HEAD -> main) Update contribution process and add helper script from Johann
* 8793a94 Add contribution information
* 4bc6f88 (tag: 2024-03-20, origin/6-release-code, 6-release-code) Set DOI and release date
* 9f8668a (origin/5-prepare-citation, 5-prepare-citation) Add citation hint and main contributors
* c9ea3e3 (origin/4-add-license-information, 4-add-license-information) Add license and copyright information
* 5971764 Record license information of dependencies
* a345e7d (origin/3-add-documentation, 3-add-documentation) Add basic description and usage information
| * f25a0f2 (origin/main) Update contribution process and add helper script from Johann # only the remote main still has to be fixed
| * 7875430 Add contribution information
| * f98c126 Set DOI and release date
| * c4e8390 Add citation hint and main contributors
| * 5c88e04 Add license and copyright information
| * 53c87c6 Record license information of dependencies
| * fe37c56 Add basic description and usage information
|/
* e73c60d (origin/2-clean-up-code, 2-clean-up-code) Add a simple pipeline to check the code on every commit
* 33235b0 Clean up code in various ways
* 3715c50 Check code via linter, fix errors and lock dependencies
* fdfa30f Organize directory structure in accordance to a data analysis layout
* 6e47c5b Add a simple test script
* 589619f (origin/1-put-into-git, 1-put-into-git) Ignore temporary Python files and only allow plot files in the results folder
* 32b1364 Move plot files into a separate folder
* 3b8ad09 (origin/0-original-source, 0-original-source) Add initial version
```
- Run `git push -f main` to conclude the changes. You might have to unprotect the branch in the GitLab project.Please double check everything at this point!
- Run `git log --graph --all --oneline --decorate` which should show a linear history again:
```
* d986098 (HEAD -> main, origin/main) Update contribution process and add helper script from Johann
* 8793a94 Add contribution information
* 4bc6f88 (tag: 2024-03-20, origin/6-release-code, 6-release-code) Set DOI and release date
* 9f8668a (origin/5-prepare-citation, 5-prepare-citation) Add citation hint and main contributors
* c9ea3e3 (origin/4-add-license-information, 4-add-license-information) Add license and copyright information
* 5971764 Record license information of dependencies
* a345e7d (origin/3-add-documentation, 3-add-documentation) Add basic description and usage information
* e73c60d (origin/2-clean-up-code, 2-clean-up-code) Add a simple pipeline to check the code on every commit
* 33235b0 Clean up code in various ways
* 3715c50 Check code via linter, fix errors and lock dependencies
* fdfa30f Organize directory structure in accordance to a data analysis layout
* 6e47c5b Add a simple test script
* 589619f (origin/1-put-into-git, 1-put-into-git) Ignore temporary Python files and only allow plot files in the results folder
* 32b1364 Move plot files into a separate folder
* 3b8ad09 (origin/0-original-source, 0-original-source) Add initial version
```
- Congratulations that you made it this far. :)
  Optionally, you can now run `git gc` to remove artifacts no loger required.